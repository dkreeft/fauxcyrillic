# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Possibility to convert files containing text
- Possibility to convert some combinations of letters into different faux Cyrillic

## [0.0.1] - 2020-05-31

### Added

- Setup project, including .gitignore, CONTRIBUTING, README, LICENSE, setup.py, setup.cfg files
- Command-line utility for converting Latin text to faux Cyrillic
