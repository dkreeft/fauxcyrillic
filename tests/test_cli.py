from click.testing import CliRunner
import fauxcyrillic.cli as fc
import fauxcyrillic.utils as ut


def test_main():
    runner = CliRunner()
    result = runner.invoke(fc.main, ['Test'])
    assert result.output
    assert result.exit_code == 0


def test_string():
    runner = CliRunner()
    str_to_convert = 'Yes, sir!'
    expectation = ut.convert(str_to_convert)
    result = runner.invoke(fc.main, [str_to_convert])
    output = result.output.rstrip('\n')
    assert result.exit_code == 0
    assert output == expectation
